document.addEventListener("DOMContentLoaded", () => {
  welcome();
});

function welcome() {
  const title = document.querySelector("#Welcome");
  title.textContent = "Welcome back, Mike!";
}

function changeButton() {
  const button = document.querySelector(".colorChange");
  let green = false;

  function switchColor() {
    button.classList.toggle("green");
    green = !green;
  }
  return switchColor;
}

const changeColor = changeButton();
